namespace BasketService.Interfaces;

public interface IBasketSessionStorage
{
    void SaveBasket(object value);
    List<Services.BasketLine> LoadBasket();
}