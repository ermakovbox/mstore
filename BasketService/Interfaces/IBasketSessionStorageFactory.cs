namespace BasketService.Interfaces;

public interface IBasketSessionStorageFactory
{
    IBasketSessionStorage Create();
}