using BasketService.Services;

namespace BasketService.Interfaces;

public interface IBasketManager
{
    void AddToBasket(Product product, int quantity);
    IList<BasketLine> GetBasketLines();
}