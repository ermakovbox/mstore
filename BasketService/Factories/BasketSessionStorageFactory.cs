using System.Security.Claims;
using BasketService.Interfaces;
using BasketService.Services;

namespace BasketService.Factories;

public class BasketSessionStorageFactory : IBasketSessionStorageFactory
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public BasketSessionStorageFactory(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }
    
    public IBasketSessionStorage Create()
    {
        var httpContext = _httpContextAccessor.HttpContext;
        var userId = httpContext?.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
        var storage = new BasketSessionStorage(_httpContextAccessor);
        storage.SetBasketKey(userId);
        return storage;
    }
}