using System.Text;
using BasketService.Factories;
using BasketService.Interceptors;
using BasketService.Interfaces;
using BasketService.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDistributedMemoryCache(); //добавляем кеш в приложение
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30); // Время жизни сессии
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
}); //добавление сохранения сессии пользователя

builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IBasketSessionStorageFactory, BasketSessionStorageFactory>();
builder.Services.AddScoped<IBasketManager, BasketManager>();
builder.Services.AddScoped<IBasketSessionStorage, BasketSessionStorage>();

builder.Services.AddHttpClient();
builder.Services.AddAuthorization();
builder.Services.AddAuthentication("Bearer")
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = "https://localhost:5001"; // Адрес IdentityServer
        options.RequireHttpsMetadata = true; // Используйте true в продакшене
        options.Audience = "BasketApi"; // Идентификатор(apiresource) вашего API, должен соответствовать одному из настроенных в IdentityServer
        
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("verySecretKeyWhichShouldBeLongEnough")),
            ValidateIssuer = true,
            ValidIssuer = "https://localhost:5001",
            ValidateAudience = true,
            ValidAudience = "BasketApi",
            ClockSkew = TimeSpan.Zero
        };
        
        
        options.Events = new JwtBearerEvents
        {
            OnAuthenticationFailed = context =>
            {
                // Логгирование ошибки аутентификации
                Console.WriteLine("Authentication failed: " + context.Exception.Message);
                return Task.CompletedTask;
            },
            OnTokenValidated = context =>
            {
                // Логгирование успешной валидации токена
                Console.WriteLine("Token validated.");
                return Task.CompletedTask;
            }
        };

        options.SaveToken = true; //токен будет сохранен в AuthenticationProperties;
    });

builder.Services.AddGrpc(options =>
{
    options.Interceptors.Add<AuthTokenLoggingInterceptor>();
});

var app = builder.Build();

app.UseAuthentication(); // Добавляет аутентификацию
app.UseAuthorization(); // Добавляет авторизацию

// Configure the HTTP request pipeline.
app.MapGrpcService<BasketApiService>();
app.MapGet("/",
    () =>
        "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.UseSession(); //включения фукции сессии

app.Run();