using BasketService.Interfaces;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;

namespace BasketService.Services;

[Authorize]
public class BasketApiService : BasketService.BasketServiceBase
{
    private readonly IBasketManager _basketManager;

    public BasketApiService(IBasketManager basketManager)
    {
        _basketManager = basketManager;
    }
    
    public override Task<BasketLinesResponse> AddProductToBasket(AddProductToBasketRequest request, ServerCallContext context)
    {
        _basketManager.AddToBasket(request.Product, request.Quantity);
        
        var lines = _basketManager.GetBasketLines().ToList();
        
        var response = new BasketLinesResponse();

        response.Lines.AddRange(lines.Select(line => new BasketLineResponse()
        {
            // Копируем свойства из line в новый BasketService.BasketLine
            Product = line.Product,
            Quantity = line.Quantity
        }));


        return Task.FromResult(response);
    }
}