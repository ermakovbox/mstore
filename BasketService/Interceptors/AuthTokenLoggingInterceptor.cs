using Grpc.Core;
using Grpc.Core.Interceptors;

namespace BasketService.Interceptors;

public class AuthTokenLoggingInterceptor : Interceptor
{
    public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
        TRequest request,
        ServerCallContext context,
        UnaryServerMethod<TRequest, TResponse> continuation)
    {
        // Пытаемся получить токен аутентификации из метаданных запроса
        var token = context.RequestHeaders.Get("authorization")?.Value;

        if (!string.IsNullOrEmpty(token))
        {
            // Логируем полученный токен (или его часть для безопасности)
            Console.WriteLine($"Received token: {token.Substring(0, Math.Min(token.Length, 30))}...");
        }
        else
        {
            Console.WriteLine("No authentication token was found in the request.");
        }

        // Продолжаем обработку запроса
        return await continuation(request, context);
    }
}