using System.Text;
using BasketApi.Factories;
using BasketApi.Interfaces;
using BasketApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "JWT Authorization header using the Bearer scheme",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                },
                Scheme = "oauth2",
                Name = "Bearer",
                In = ParameterLocation.Header,
            },
            new List<string>()
        }
    });
});

// Add services to the container.
builder.Services.AddDistributedMemoryCache(); //добавляем кеш в приложение
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30); // Время жизни сессии
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
}); //добавление сохранения сессии пользователя

builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IBasketSessionStorageFactory, BasketSessionStorageFactory>();
builder.Services.AddScoped<IBasketManager, BasketManager>();
builder.Services.AddScoped<IBasketSessionStorage, BasketSessionStorage>();

builder.Services.AddHttpClient();
builder.Services.AddAuthentication("Bearer")
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = "https://localhost:5001"; // Адрес IdentityServer
        options.RequireHttpsMetadata = true; // Используйте true в продакшене
        options.Audience = "BasketApi"; // Идентификатор(apiresource) вашего API, должен соответствовать одному из настроенных в IdentityServer
        
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("verySecretKeyWhichShouldBeLongEnough")),
            ValidateIssuer = true,
            ValidIssuer = "https://localhost:5001",
            ValidateAudience = true,
            ValidAudience = "BasketApi",
            ClockSkew = TimeSpan.Zero
        };
        
        
        options.Events = new JwtBearerEvents
        {
            OnAuthenticationFailed = context =>
            {
                // Логгирование ошибки аутентификации
                Console.WriteLine("Authentication failed: " + context.Exception.Message);
                return Task.CompletedTask;
            },
            OnTokenValidated = context =>
            {
                // Логгирование успешной валидации токена
                Console.WriteLine("Token validated.");
                return Task.CompletedTask;
            }
        };

        options.SaveToken = true; //токен будет сохранен в AuthenticationProperties;
    });

var app = builder.Build();

app.UseAuthentication(); // Добавляет аутентификацию
app.UseAuthorization(); // Добавляет авторизацию

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSession(); //включения фукции сессии
app.UseHttpsRedirection();

app.MapControllers();

app.Run();