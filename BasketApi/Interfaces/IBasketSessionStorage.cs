using BasketApi.Services;

namespace BasketApi.Interfaces;

public interface IBasketSessionStorage
{
    void SaveBasket(object value);
    List<BasketLine> LoadBasket();
}