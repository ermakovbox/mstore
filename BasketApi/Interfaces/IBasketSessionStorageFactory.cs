namespace BasketApi.Interfaces;

public interface IBasketSessionStorageFactory
{
    IBasketSessionStorage Create();
}