using BasketApi.Services;
using ProductsLibary.Models;

namespace BasketApi.Interfaces;

public interface IBasketManager
{
    void AddToBasket(Product product, int quantity);
    IList<BasketLine> GetBasketLines();
}