using BasketApi.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductsLibary.Models;

namespace BasketApi.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class BasketController : ControllerBase
{
    private readonly IBasketManager _basketManage;

    public BasketController(IBasketManager basketManage)
    {
        _basketManage = basketManage;
    }

    [HttpPost("add-to-basket")]
    public IActionResult AddToBasket(Product product, int quantity = 1)
    {
        _basketManage.AddToBasket(product, quantity);
        
        return Ok(_basketManage.GetBasketLines());
    }
    
    [HttpGet("get-basket")]
    public IActionResult GetBasket()
    {
        return Ok(_basketManage.GetBasketLines());
    }
}