using BasketApi.Extensions;
using BasketApi.Interfaces;

namespace BasketApi.Services;

public class BasketSessionStorage : IBasketSessionStorage
{
    private readonly ISession? _session;
    private string BasketKey { get; set; } = null!;

    public BasketSessionStorage(IHttpContextAccessor httpContextAccessor)
    {
        _session = httpContextAccessor.HttpContext?.Session;
    }

    public void SaveBasket(object value)
    {
        _session?.SetJson(BasketKey, value);
    }

    public List<BasketLine> LoadBasket()
    {
        return _session is not null 
            ? _session.GetJson<List<BasketLine>>(BasketKey) ?? new List<BasketLine>() 
            : new List<BasketLine>();
    }
    
    public void SetBasketKey(string? userId)
    {
        BasketKey = userId != null ? $"Basket_{userId}" : $"Basket_{_session?.Id}";
    }
}