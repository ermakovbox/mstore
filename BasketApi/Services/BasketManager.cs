using BasketApi.Interfaces;
using ProductsLibary.Models;

namespace BasketApi.Services;

public sealed class BasketManager : IBasketManager
{
    private readonly IBasketSessionStorage _sessionManager;
    private List<BasketLine> Lines { get; set; } = new();
    
    public BasketManager(IBasketSessionStorageFactory sessionStorageFactory)
    {
        _sessionManager = sessionStorageFactory.Create();
        LoadBasket();
    }

    public void AddToBasket(Product product, int quantity)
    {
        var line = Lines.FirstOrDefault(p => p.Product.Id == product.Id);
        if (line == null)
        {
            Lines.Add(new BasketLine
            {
                Product = product,
                Quantity = quantity
            });
        }
        else
        {
            line.Quantity += quantity;
        }
        
        SaveBasket();
    }
    
    public IList<BasketLine> GetBasketLines()
    {
        return Lines;
    }
    
    private void LoadBasket()
    {
        Lines = _sessionManager.LoadBasket();
    }

    private void SaveBasket()
    {
        _sessionManager.SaveBasket(Lines);
    }
}

public sealed class BasketLine
{
    public Product Product { get; init; } = null!;
    public int Quantity { get; set; }
}