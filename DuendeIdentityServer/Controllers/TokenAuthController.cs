using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using DuendeIdentityServer.DataAccess;
using DuendeIdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace DuendeIdentityServer.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TokenAuthController : ControllerBase
{
    private readonly IConfiguration _configuration;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly EFIdentityContext _context;
    
    public TokenAuthController(UserManager<ApplicationUser> userManager,
        IConfiguration configuration, 
        RoleManager<IdentityRole> roleManager, 
        EFIdentityContext context)
    {
        _userManager = userManager;
        _configuration = configuration;
        _roleManager = roleManager;
        _context = context;
    }

    [HttpPost("register")]
    public async Task<IActionResult> RegisterAsync([FromBody] InputAuthModel model)
    {
        await using var transaction = await _context.Database.BeginTransactionAsync();
        try
        {
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                EmailConfirmed = true
            };
            
            var createUserResult = await _userManager.CreateAsync(user, model.Password);

            if (!createUserResult.Succeeded)
            {
                return BadRequest(createUserResult.Errors);
            }
            
            var userRoles = new List<string> {"Manager", "Admin"};

            foreach (var roleName in userRoles)
            {
                await _userManager.AddToRoleAsync(user, roleName);
            }

            await transaction.CommitAsync();
            
            return Ok(user);
        }
        catch (Exception ex)
        {
            // Откат в случае ошибки
            await transaction.RollbackAsync();
            return StatusCode(500, "Internal server error: " + ex.Message);
        }
    }
    
    [HttpPost("login")]
    public async Task<IActionResult> LoginAsync([FromBody] InputAuthModel model)
    {
        var user = await _userManager.FindByNameAsync(model.Email);
        if (user is null || !await _userManager.CheckPasswordAsync(user, model.Password))
        {
            return Unauthorized();
        }

        var authClaims = await CreateClaimsAsync(user);
        var token = CreateJwtToken(authClaims);
        var tokenHandler = new JwtSecurityTokenHandler();

        return Ok(new
        {
            token = tokenHandler.WriteToken(token),
            expires = token.ValidTo
        });
    }
    
    private async Task<IList<Claim>> CreateClaimsAsync(ApplicationUser user)
    {
        if (user.UserName is null || user.Email is null) throw new ArgumentNullException(nameof(user));
            
        var authClaims = new List<Claim>
        {
            new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
            new Claim(JwtRegisteredClaimNames.Email, user.Email),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new Claim(ClaimTypes.NameIdentifier, user.Id)
        };
        
        var roles = await _userManager.GetRolesAsync(user);
        authClaims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));
        
        return authClaims;
    }
    
    private SecurityToken CreateJwtToken(IList<Claim> claims)
    {
        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        
        var tokenDescriptor = new SecurityTokenDescriptor()
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.Now.AddMinutes(10),
            SigningCredentials = creds,
            Issuer = _configuration["Jwt:Issuer"],
            Audience = _configuration["Jwt:Audience"]
        };
        
        var tokenHandler = new JwtSecurityTokenHandler();
        return tokenHandler.CreateToken(tokenDescriptor);
    }
    
    [HttpGet ("data")]
    public IActionResult Data()
    {
        var user = User.Identity as ClaimsIdentity;
        
        var claims = (user?.Claims ?? Array.Empty<Claim>()).ToDictionary(claim => claim.Type, claim => claim.Value);

        return Ok(claims);
    }

}