using Duende.IdentityServer.EntityFramework.DbContexts;
using Duende.IdentityServer.EntityFramework.Mappers;
using Microsoft.EntityFrameworkCore;

namespace DuendeIdentityServer;

internal static class HostingExtensions
{
    public static void InitializeDatabase(IApplicationBuilder app)
    {
        using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()?.CreateScope();
        
        if (serviceScope is null) return;
        
        serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

        var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
        context.Database.Migrate();
        
        if (Config.Clients.Any())
        {
            foreach (var client in Config.Clients)
            {
                if (context.Clients.Any(x => x.ClientId == client.ClientId)) continue;
                context.Clients.Add(client.ToEntity());
            }

            context.SaveChanges();
        }

        if (Config.IdentityResources.Any())
        {
            foreach (var resource in Config.IdentityResources)
            {
                if (context.IdentityResources.Any(x => x.Name == resource.Name)) continue;
                context.IdentityResources.Add(resource.ToEntity());
            }

            context.SaveChanges();
        }

        if (Config.ApiScopes.Any())
        {
            foreach (var resource in Config.ApiScopes)
            {
                if (context.ApiScopes.Any(x => x.Name == resource.Name)) continue;
                context.ApiScopes.Add(resource.ToEntity());
            }

            context.SaveChanges();
        }
        
        if (Config.ApiResources.Any())
        {
            foreach (var resource in Config.ApiResources)
            {
                if (context.ApiResources.Any(x => x.Name == resource.Name)) continue;
                context.ApiResources.Add(resource.ToEntity());
            }

            context.SaveChanges();
        }
    }
}