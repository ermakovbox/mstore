using System.Text;
using DuendeIdentityServer;
using DuendeIdentityServer.DataAccess;
using DuendeIdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Logging;

var builder = WebApplication.CreateBuilder(args);
IdentityModelEventSource.ShowPII = true; // Включаем логирование PII (лично идентифицируемая информация)
const string defaultConnectionString = "ConnectionStrings:DefaultConnection";

builder.Services.AddControllers();

builder.Services.AddDbContext<EFIdentityContext>(options =>
{
    options.UseNpgsql(builder.Configuration[defaultConnectionString], 
        x => x.MigrationsAssembly(typeof(Program).Assembly.FullName));
});

builder.Services.AddIdentity<ApplicationUser, IdentityRole>()
    .AddEntityFrameworkStores<EFIdentityContext>()
    .AddDefaultTokenProviders();

builder.Services.AddIdentityServer()
    .AddAspNetIdentity<ApplicationUser>()
    // Настройка хранения конфигурации в базе данных
    .AddConfigurationStore(options =>
    {
        options.ConfigureDbContext = b => b.UseNpgsql(
            builder.Configuration[defaultConnectionString],
            sql => sql.MigrationsAssembly(typeof(Program).Assembly.FullName));
    })
    // Настройка хранения операционных данных в базе данных
    .AddOperationalStore(options =>
    {
        options.ConfigureDbContext = b => b.UseNpgsql(
            builder.Configuration[defaultConnectionString],
            sql => sql.MigrationsAssembly(typeof(Program).Assembly.FullName));
    })
    .AddDeveloperSigningCredential(); //это только в целях разработки

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Настройка логирования
builder.Logging.ClearProviders();
builder.Logging.AddConsole();

var app = builder.Build();

app.Use(async (context, next) =>
{
    if (context.Request.Path.StartsWithSegments("/connect/introspect"))
    {
        var body = await new StreamReader(context.Request.Body).ReadToEndAsync();
        
        // Восстанавливаем поток для дальнейшего чтения IdentityServer
        context.Request.Body = new MemoryStream(Encoding.UTF8.GetBytes(body));

        Console.WriteLine($"Introspection request body: {body}");
    }

    await next.Invoke();
});


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

HostingExtensions.InitializeDatabase(app);

app.UseHttpsRedirection();
app.UseIdentityServer();

app.MapControllers();

app.Run();