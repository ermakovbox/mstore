using DuendeIdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DuendeIdentityServer.DataAccess;

public class EFIdentityContext : IdentityDbContext
{
    public EFIdentityContext(DbContextOptions<EFIdentityContext> options) : base(options) { }

    public DbSet<ApplicationUser> ApplicationUsers => Set<ApplicationUser>();
    public DbSet<IdentityRole> IdentityRoles => Set<IdentityRole>();
}