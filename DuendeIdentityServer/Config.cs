﻿using Duende.IdentityServer;
using Duende.IdentityServer.Models;

namespace DuendeIdentityServer;

public static class Config
{
    public static IEnumerable<IdentityResource> IdentityResources =>
        new List<IdentityResource>
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
        };
    
    public static IEnumerable<ApiResource> ApiResources =>
        new List<ApiResource>
        {
            new ApiResource("BasketApi", "Basket Api")
            {
                Scopes = new[] { "BasketApi.Read", "BasketApi.Write"}
            },
            new ApiResource("CatalogApi", "Catalog Api")
            {
                Scopes = new[] { "CatalogApi.Read", "CatalogApi.Write"}
            }
        };


    public static IEnumerable<ApiScope> ApiScopes =>
        new List<ApiScope>
        {
            new ApiScope("api1", "My API"),
            new ApiScope("BasketApi.Read", "Read access to Basket Api"),
            new ApiScope("BasketApi.Write", "Write access to Basket Api"),
            new ApiScope("CatalogApi.Read", "Read access to Catalog Api"),
            new ApiScope("CatalogApi.Write", "Write access to Catalog Api")
        };

    public static IEnumerable<Client> Clients =>
        new List<Client>
        {
            // machine to machine client
            new Client
            {
                ClientId = "client",
                ClientSecrets = { new Secret("secret".Sha256()) },

                AllowedGrantTypes = GrantTypes.ClientCredentials,
                // scopes that client has access to
                AllowedScopes =
                {
                    "BasketApi.Read", 
                    "BasketApi.Write", 
                    "CatalogApi.Read", 
                    "CatalogApi.Write"
                }
            },
                
            // interactive ASP.NET Core Web App
            new Client
            {
                ClientId = "web",
                ClientSecrets = { new Secret("secret".Sha256()) },

                AllowedGrantTypes = GrantTypes.Code,
                    
                // where to redirect to after login
                RedirectUris = { "https://localhost:5001/signin-oidc" },

                // where to redirect to after logout
                PostLogoutRedirectUris = { "https://localhost:5001/signout-callback-oidc" },
    
                AllowOfflineAccess = true,

                AllowedScopes = new List<string>
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    "BasketApi.Read",
                    "BasketApi.Write",
                    "CatalogApi.Read",
                    "CatalogApi.Write"
                }
            }
        };
}
