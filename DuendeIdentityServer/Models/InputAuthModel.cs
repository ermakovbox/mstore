namespace DuendeIdentityServer.Models;

public class InputAuthModel
{
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;
    public bool RememberMe { get; set; }
}