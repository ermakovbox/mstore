﻿// See https://aka.ms/new-console-template for more information
using BasketClient;
using CatalogClient;
using Grpc.Net.Client;
using ProductGrps = BasketClient.Product;
using Product = ProductsLibary.Models.Product;

using var channelOne = GrpcChannel.ForAddress("https://localhost:7131");
using var channelTwo = GrpcChannel.ForAddress("https://localhost:7298");

var clientOne = new BasketService.BasketServiceClient(channelOne);
var clientTwo = new CatalogService.CatalogServiceClient(channelTwo);

var getProductsRequest = new GetProductsRequest
{
    Name = "Pr"
};

Product? _product = null;

try
{
    var responseProducts = await clientTwo.GetProductsByNameAsync(request: getProductsRequest);

    if (responseProducts.Products.Count > 0)
    {
        var product = responseProducts.Products.FirstOrDefault();
        if (product != null)
        {
            _product = new Product
            {
                Id = Guid.Parse(product.Id),
                Name = product.Name,
                Description = product.Description,
                Price = (decimal)product.Price
            };
            Console.WriteLine("Product found: " + _product.Name);
        }
    }
    else
    {
        Console.WriteLine("No products found.");
    }
}
catch (Exception e)
{
    Console.WriteLine(e);
    throw;
}

while (true)
{
    if (_product is not null)
    {
        var quantity = Console.ReadLine();

        if (quantity != null)
        {
            var request = new AddProductToBasketRequest
            {
                Product = new ProductGrps
                {
                    Id = _product.Id.ToString(),
                    Name = _product.Name,
                    Description = _product.Description,
                    Price = (double)_product.Price
                },
                Quantity = int.Parse(quantity)
            };

            var response = await clientOne.AddProductToBasketAsync(request: request);
            if (response.Lines.Count > 0)
            {
                Console.WriteLine("Basket contains:" + response.Lines);
                Console.WriteLine("Product added to basket successfully.");
            }
            else
            {
                Console.WriteLine("Failed to add product to basket.");
            }
        }

        await channelOne.ShutdownAsync();
    }
}