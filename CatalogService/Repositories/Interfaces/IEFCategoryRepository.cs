namespace CatalogService.Repositories.Interfaces;

public interface IEFCategoryRepository
{
    public Task CreateCategoryAsync(Models.DataModels.Category category);
    
    public Task<Models.DataModels.Category?> GetCategoryByIdAsync(int id);
    
    public Task<IEnumerable<Models.DataModels.Category>> GetCategoriesByIdsAsync(IEnumerable<int> ids);
    
    public Task<IEnumerable<Models.DataModels.Category>> GetCategoriesByBrandIdAsync(int brandId);
    
    public Task<IEnumerable<Models.DataModels.Category>> GetCategoriesByBrandNameAsync(string name);
    
    public Task AddCategoryToManufactureAsync(int manufactureId, Models.DataModels.Category category);

    public Task AddCategoryToManufactureAsync(int manufactureId, int categoryId);
}