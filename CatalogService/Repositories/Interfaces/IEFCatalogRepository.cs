namespace CatalogService.Repositories.Interfaces;

public interface IEFCatalogRepository
{
    public Task AddProductAsync(ProductsLibary.Models.Product product);
    
    public Task<IList<Product>> GetProductsAsync();

    public Task<ProductsLibary.Models.Product?> GetProductByIdAsync(Guid id);
}