namespace CatalogService.Repositories.Interfaces;

public interface IEFProductRepository
{
    public Task CreateProductAsync(Models.DataModels.Product product);
    public Task<Models.DataModels.Product?> GetProductByIdAsync(Ulid id);
    public Task<Models.DataModels.Product> GetFullProductByIdAsync(Ulid id);
    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByCategoryIdAsync(int categoryId);
    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByCategoryNameAsync(string name);
    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByBrandIdAsync(int brandId);
    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByBrandNameAsync(string name);
}