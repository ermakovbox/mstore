namespace CatalogService.Repositories.Interfaces;

public interface IEFManufactureRepository
{
    public Task CreateManufactureAsync(Models.DataModels.Manufacture manufacture);
    public Task<Models.DataModels.Manufacture?> GetManufactureByIdAsync(int id);
    public Task<IEnumerable<Models.DataModels.Manufacture>> GetManufacturesAsync(string name);
    public Task UpdateManufactureAsync(Manufacture manufacture);
    public Task DeleteManufactureAsync(int id);
}