namespace CatalogService.Repositories.Interfaces;

public interface IEFProductVariationRepository
{
    public Task CreateProductVariationAsync(Models.DataModels.ProductVariation productVariation);
    
    public Task<Models.DataModels.ProductVariation> GetProductVariationByIdAsync(Ulid id);
    
    public Task<IEnumerable<Models.DataModels.ProductVariation>> GetProductVariationsByProductIdAsync(Ulid productId);
}