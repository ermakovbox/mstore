using CatalogService.DataAccess;
using CatalogService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CatalogService.Repositories;

public class EFManufactureRepository : IEFManufactureRepository
{
    private readonly EFCatalogContext _context;

    public EFManufactureRepository(EFCatalogContext context)
    {
        _context = context;
    }

    public async Task CreateManufactureAsync(Models.DataModels.Manufacture manufacture)
    {
        await _context.AddAsync(manufacture);
        await _context.SaveChangesAsync();
    }

    public async Task<Models.DataModels.Manufacture?> GetManufactureByIdAsync(int id)
    {
        var manufacture = await _context.DataManufactures.Include(m => m.Categories)
            .FirstOrDefaultAsync(m => m.Id == id);
        
        return manufacture;
    }

    public async Task<IEnumerable<Models.DataModels.Manufacture>> GetManufacturesAsync(string name)
    {
        return await _context.DataManufactures.Where(m => m.Name.StartsWith(name)).ToListAsync();
    }

    public Task UpdateManufactureAsync(Manufacture manufacture)
    {
        throw new NotImplementedException();
    }

    public Task DeleteManufactureAsync(int id)
    {
        throw new NotImplementedException();
    }
}