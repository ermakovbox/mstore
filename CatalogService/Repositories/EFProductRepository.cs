using CatalogService.DataAccess;
using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace CatalogService.Repositories;

public class EFProductRepository : IEFProductRepository
{
    private readonly EFCatalogContext _context;

    public EFProductRepository(EFCatalogContext context)
    {
        _context = context;
    }

    public async Task CreateProductAsync(Models.DataModels.Product product)
    {
        await _context.AddAsync(product);
        await _context.SaveChangesAsync();
    }
    
    public async Task<Models.DataModels.Product?> GetProductByIdAsync(Ulid id)
    {
        return await _context.DataProducts.FirstOrDefaultAsync(p => p.Id == id);
    }

    public async Task<Models.DataModels.Product> GetFullProductByIdAsync(Ulid id)
    {
        return await _context.DataProducts.Include(product => product.Categories)
                   .Include(product => product.Variations)
                   .FirstOrDefaultAsync(product => product.Id == id) 
               ?? throw new RpcException(new Status(StatusCode.NotFound, $"Product with id {id} not found"));
    }

    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByCategoryIdAsync(int categoryId)
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByCategoryNameAsync(string name)
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByBrandIdAsync(int brandId)
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<Models.DataModels.Product>> GetProductsByBrandNameAsync(string name)
    {
        throw new NotImplementedException();
    }
}