using CatalogService.DataAccess;
using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace CatalogService.Repositories;

public class EFProductVariationRepository : IEFProductVariationRepository
{
    private readonly EFCatalogContext _context;

    public EFProductVariationRepository(EFCatalogContext context)
    {
        _context = context;
    }

    public async Task CreateProductVariationAsync(Models.DataModels.ProductVariation productVariation)
    {
        await _context.AddAsync(productVariation);
        await _context.SaveChangesAsync();
    }

    public async Task<Models.DataModels.ProductVariation> GetProductVariationByIdAsync(Ulid id)
    {
        return await _context.DataProductVariations.FirstOrDefaultAsync(pv => pv.Id == id)
            ?? throw new RpcException(new Status(StatusCode.NotFound, $"ProductVariation with id {id} not found"));
    }

    public async Task<IEnumerable<Models.DataModels.ProductVariation>> GetProductVariationsByProductIdAsync(Ulid productId)
    {
        return await _context.DataProductVariations.Where(pv => pv.ProductId == productId).ToListAsync();
    }
}