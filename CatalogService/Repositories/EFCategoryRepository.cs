using CatalogService.DataAccess;
using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace CatalogService.Repositories;

public class EFCategoryRepository : IEFCategoryRepository
{
    private readonly EFCatalogContext _context;

    public EFCategoryRepository(EFCatalogContext context)
    {
        _context = context;
    }

    public async Task CreateCategoryAsync(Models.DataModels.Category category)
    {
        await _context.AddAsync(category);
        await _context.SaveChangesAsync();
    }

    public async Task<Models.DataModels.Category?> GetCategoryByIdAsync(int id)
    {
        return await _context.DataCategories.FirstOrDefaultAsync(c => c.Id == id);
    }
    
    public async Task<IEnumerable<Models.DataModels.Category>> GetCategoriesByIdsAsync(IEnumerable<int> ids)
    {
        var existingIds = await _context.DataCategories
            .Select(c => c.Id)
            .Where(id => ids.Contains(id))
            .ToListAsync();

        var missingIds = ids.Except(existingIds).ToList();
        if (missingIds.Count != 0)
        {
            throw new RpcException(new Status(StatusCode.NotFound, $"Category id {string.Join(", ", missingIds)} not found"));
        }

        return await _context.DataCategories.Where(c => existingIds.Contains(c.Id)).ToListAsync();
    }

    public async Task<IEnumerable<Models.DataModels.Category>> GetCategoriesByBrandIdAsync(int brandId)
    {
        return (await _context.DataManufactures.Include(m => m.Categories)
            .FirstOrDefaultAsync(m => m.Id == brandId))?.Categories 
               ?? Enumerable.Empty<Models.DataModels.Category>();
    }

    public async Task<IEnumerable<Models.DataModels.Category>> GetCategoriesByBrandNameAsync(string name)
    {
        return (await _context.DataManufactures.Include(m => m.Categories)
            .FirstOrDefaultAsync(m => m.Name == name))?.Categories
               ?? Enumerable.Empty<Models.DataModels.Category>();
    }
    
    public async Task AddCategoryToManufactureAsync(int manufactureId, Models.DataModels.Category category)
    {
        var manufacture = await _context.DataManufactures.Include(m => m.Categories)
            .FirstAsync(m => m.Id == manufactureId) 
                          ?? throw new InvalidOperationException("Manufacture not found.");
        
        manufacture.Categories.Add(category);
        await _context.SaveChangesAsync();
    }
    
    public async Task AddCategoryToManufactureAsync(int manufactureId, int categoryId)
    {
        var manufacture = await _context.DataManufactures.Include(m => m.Categories)
            .FirstAsync(m => m.Id == manufactureId) 
                          ?? throw new InvalidOperationException("Manufacture not found.");
        
        var category = await GetCategoryByIdAsync(categoryId) 
                       ?? throw new InvalidOperationException("Category not found.");
        
        manufacture.Categories.Add(category);
        await _context.SaveChangesAsync();
    }
}