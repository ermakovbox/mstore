using CatalogService.DataAccess;
using CatalogService.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CatalogService.Repositories;

public class EFCatalogRepository : IEFCatalogRepository
{
    private readonly EFCatalogContext _context;

    public EFCatalogRepository(EFCatalogContext context)
    {
        _context = context;
    }
    
    public async Task AddProductAsync(ProductsLibary.Models.Product product)
    {
        await _context.Products.AddAsync(product);
        await _context.SaveChangesAsync();
    }
    
    public async Task<IList<Product>> GetProductsAsync()
    {
        return await _context.Products.Select(p => new Product
        {
            Id = p.Id.ToString(), // Преобразование Guid в строку
            Name = p.Name,
            Description = p.Description,
            Price = (double)p.Price
        }).ToListAsync();
    }
    
    public async Task<ProductsLibary.Models.Product?> GetProductByIdAsync(Guid id)
    {
        return await _context.Products.FindAsync(id);
    }
}