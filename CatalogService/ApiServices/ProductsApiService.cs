using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;

namespace CatalogService.ApiServices;

[Authorize]
public class ProductsApiService : ProductsService.ProductsServiceBase
{
    private readonly IEFProductRepository _productRepository;
    private readonly IEFCategoryRepository _categoryRepository;

    public ProductsApiService(IEFProductRepository productRepository, 
        IEFCategoryRepository categoryRepository)
    {
        _productRepository = productRepository;
        _categoryRepository = categoryRepository;
    }
    
    public override async Task<CreateProductBaseResponse> CreateProductBase(CreateProductBaseRequest request, ServerCallContext context)
    {
        var categories = (await _categoryRepository.GetCategoriesByIdsAsync(request.CategoryIds)).ToList();
        
        var product = new Models.DataModels.Product
        {
            Id = Ulid.NewUlid(),
            Name = request.Name,
            Description = request.Description,
            Categories = categories
        };
        
        await _productRepository.CreateProductAsync(product);
        
        var checkProduct = await _productRepository.GetProductByIdAsync(product.Id);
        if (checkProduct is null)
        {
            throw new RpcException(new Status(StatusCode.Internal, "Product not created"));
        }
        
        var response = new CreateProductBaseResponse
        {
            Product = new ProductBase()
            {
                Id = checkProduct.Id.ToString(),
                Name = checkProduct.Name,
                Description = checkProduct.Description,
                Categories = { checkProduct.Categories.Select(c => new Category()
                {
                    Id = c.Id,
                    Name = c.Name
                }) }
            }
        };

        return response;
    }

    public override async Task<GetFullProductByIdResponse> GetFullProductById(GetFullProductByIdRequest request, ServerCallContext context)
    {
        var product = await _productRepository.GetFullProductByIdAsync(Ulid.Parse(request.Id));
        
        var response = new GetFullProductByIdResponse
        {
            Product = new FullProduct
            {
                Product = new ProductBase()
                {
                    Id = product.Id.ToString(),
                    Name = product.Name,
                    Description = product.Description,
                    Categories = { product.Categories.Select(c => new Category()
                    {
                        Id = c.Id,
                        Name = c.Name
                    }) }
                },
                Variations = { product.Variations?.Select(pv => new ProductVariation()
                {
                    Id = pv.Id.ToString(),
                    FullName = pv.FullName,
                    Image = pv.Image,
                    Price = (double)pv.Price
                }) }
            }
        };
            
        return response;
    }
}