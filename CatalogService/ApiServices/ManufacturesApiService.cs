using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;

namespace CatalogService.ApiServices;

[Authorize]
public class ManufacturesApiService : ManufacturesService.ManufacturesServiceBase
{
    private readonly IEFManufactureRepository _manufactureRepository;

    public ManufacturesApiService(IEFManufactureRepository manufactureRepository)
    {
        _manufactureRepository = manufactureRepository;
    }
    
    public override async Task<GetManufactureResponse> CreateManufacture(CreateManufactureRequest request, ServerCallContext context)
    {
        var manufacture = new Models.DataModels.Manufacture
        {
            Name = request.Name
        };
    
        await _manufactureRepository.CreateManufactureAsync(manufacture);
        
        var createdManufacture = await _manufactureRepository.GetManufactureByIdAsync(manufacture.Id) 
                                 ?? throw new RpcException(new Status(StatusCode.Internal, "Failed to create manufacture"));
        
        var response = new GetManufactureResponse
        {
            Manufacture = new Manufacture()
            {
                Id = createdManufacture.Id,
                Name = createdManufacture.Name
            }
        };

        return response;
    }
    
    public override async Task<GetManufacturesResponse> GetManufactures(GetManufacturesRequest request, ServerCallContext context)
    {
        var manufactures = await _manufactureRepository.GetManufacturesAsync(request.Name);

        var response = new GetManufacturesResponse();
        response.Manufactures.AddRange(manufactures.Select(manufacture => new Manufacture
        {
            Id = manufacture.Id,
            Name = manufacture.Name
        }));
        
        return response;
    }
    
    public override async Task<GetManufactureResponse> GetManufactureById(GetManufactureByIdRequest request, ServerCallContext context)
    {
        var manufacture = await _manufactureRepository.GetManufactureByIdAsync(request.Id) 
                          ?? throw new RpcException(new Status(StatusCode.NotFound, "Manufacture not found"));

        var response = new GetManufactureResponse
        {
            Manufacture = new Manufacture
            {
                Id = manufacture.Id,
                Name = manufacture.Name,
                Categories = { manufacture.Categories.Select(category => new Category
                {
                    Id = category.Id,
                    Name = category.Name
                }) }
            }
        };

        return response;
    }
}