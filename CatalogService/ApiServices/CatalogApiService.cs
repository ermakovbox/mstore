using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;

namespace CatalogService.ApiServices;

[Authorize]
public class CatalogApiService : CatalogService.CatalogServiceBase
{
    private readonly IEFCatalogRepository _repository;

    public CatalogApiService(IEFCatalogRepository repository)
    {
        _repository = repository;
    }

    public override async Task<GetProductsResponse> GetProductsByName(GetProductsRequest request, ServerCallContext context)
    {
        var products = await _repository.GetProductsAsync();
    
        var response = new GetProductsResponse();
    
        response.Products.AddRange(products.Select(product => new Product
        {
            Id = product.Id,
            Name = product.Name,
            Description = product.Description,
            Price = product.Price
        }));
    
        return response;
    }
    
    public override async Task<Product> GetProductById(GetProductByIdRequest request, ServerCallContext context)
    {
        var product = await _repository.GetProductByIdAsync(Guid.Parse(request.Id));
    
        if (product is null)
        {
            throw new RpcException(new Status(StatusCode.NotFound, "Product not found"));
        }
        
        var response = new Product
        {
            Id = product.Id.ToString(),
            Name = product.Name,
            Description = product.Description,
            Price = (double)product.Price
        };
    
        return response;
    }
}