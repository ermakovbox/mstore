using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;

namespace CatalogService.ApiServices;

[Authorize]
public class CategoriesApiService : CategoriesService.CategoriesServiceBase
{
    private readonly IEFCategoryRepository _categoryRepository;
    private readonly IEFManufactureRepository _manufactureRepository;

    public CategoriesApiService(IEFCategoryRepository categoryRepository, IEFManufactureRepository manufactureRepository)
    {
        _categoryRepository = categoryRepository;
        _manufactureRepository = manufactureRepository;
    }
    
    public override async Task<GetCategoryResponse> CreateCategory(CreateCategoryRequest request, ServerCallContext context)
    {
        var category = new Models.DataModels.Category
        {
            Name = request.Name
        };
    
        await _categoryRepository.CreateCategoryAsync(category);
        
        var createdCategory = await _categoryRepository.GetCategoryByIdAsync(category.Id) 
                              ?? throw new RpcException(new Status(StatusCode.Internal, "Failed to create category"));
        
        var response = new GetCategoryResponse
        {
            Category = new Category
            {
                Id = createdCategory.Id,
                Name = createdCategory.Name
            }
        };

        return response;
    }
    
    public override async Task<GetManufactureResponse> AddCategoryToManufacture(AddCategoryToManufactureRequest request, ServerCallContext context)
    {
        await _categoryRepository.AddCategoryToManufactureAsync(request.ManufactureId, request.CategoryId);
        var manufacture = await _manufactureRepository.GetManufactureByIdAsync(request.ManufactureId) 
                          ?? throw new RpcException(new Status(StatusCode.NotFound, "Manufacture not found"));
        
        var response = new GetManufactureResponse
        {
            Manufacture = new Manufacture
            {
                Id = manufacture.Id,
                Name = manufacture.Name,
                Categories = { manufacture.Categories.Select(category => new Category
                {
                    Id = category.Id,
                    Name = category.Name
                }) }
            }
        };
        
        return response;
    }
}