using CatalogService.Repositories.Interfaces;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;

namespace CatalogService.ApiServices;

[Authorize]
public class ProductVariationsApiService : ProductVariationsService.ProductVariationsServiceBase
{
    private readonly IEFProductVariationRepository _productVariationRepository;

    public ProductVariationsApiService(IEFProductVariationRepository productVariationRepository)
    {
        _productVariationRepository = productVariationRepository;
    }

    public override async Task<ProductVariation> CreateProductVariation(CreateProductVariationRequest request, ServerCallContext context)
    {
        var productVariation = new Models.DataModels.ProductVariation
        {
            Id = Ulid.NewUlid(),
            ProductId = Ulid.Parse(request.ProductId),
            FullName = request.FullName,
            Image = request.Image,
            Price = (decimal)request.Price
        };
        
        await _productVariationRepository.CreateProductVariationAsync(productVariation);
        
        var checkProductVariation = await _productVariationRepository.GetProductVariationByIdAsync(productVariation.Id) 
                                    ?? throw new RpcException(new Status(StatusCode.Internal, "Failed to create product variation"));
        
        var response = new ProductVariation
        {
            Id = checkProductVariation.Id.ToString(),
            ProductId = checkProductVariation.ProductId.ToString(),
            FullName = checkProductVariation.FullName,
            Image = checkProductVariation.Image,
            Price = (double)checkProductVariation.Price
        };
        
        return response;
    }
    
    public override async Task<ProductVariation> GetProductVariationById(GetProductVariationByIdRequest request, ServerCallContext context)
    {
        var productVariation = await _productVariationRepository.GetProductVariationByIdAsync(Ulid.Parse(request.Id)) 
                               ?? throw new RpcException(new Status(StatusCode.NotFound, "ProductVariation not found"));
        
        var response = new ProductVariation
        {
            Id = productVariation.Id.ToString(),
            ProductId = productVariation.ProductId.ToString(),
            FullName = productVariation.FullName,
            Image = productVariation.Image,
            Price = (double)productVariation.Price
        };
        
        return response;
    }
    
    public override async Task<GetProductVariationsResponse> GetProductVariationsByProductId(GetProductVariationByProductIdRequest request, ServerCallContext context)
    {
        var productVariations = await _productVariationRepository.GetProductVariationsByProductIdAsync(Ulid.Parse(request.ProductId));
        
        var response = new GetProductVariationsResponse();
        response.Variations.AddRange(productVariations.Select(productVariation => new ProductVariation
        {
            Id = productVariation.Id.ToString(),
            ProductId = productVariation.ProductId.ToString(),
            FullName = productVariation.FullName,
            Image = productVariation.Image,
            Price = (double)productVariation.Price
        }));
        
        return response;
    }
}