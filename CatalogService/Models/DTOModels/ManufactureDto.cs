using CatalogService.Models.DataModels;

namespace CatalogService.Models.DTOModels;

public record ManufactureDto
{
    public int Id { get; init; }
    public string Name { get; init; } = null!;
    
    public ICollection<Category>? Categories { get; init; }
}