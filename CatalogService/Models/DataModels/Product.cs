namespace CatalogService.Models.DataModels;

public record Product
{
    public Ulid Id { get; init; }
    public string Name { get; init; } = null!;
    public string Description { get; init; } = null!;
    
    public ICollection<Category> Categories { get; init; } = null!;
    public ICollection<ProductVariation>? Variations { get; init; }
}