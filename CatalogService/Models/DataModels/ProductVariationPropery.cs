namespace CatalogService.Models.DataModels;

public class ProductVariationProperty
{
    public Ulid Id { get; set; }
    public string Name { get; set; } = null!;
    public string Value { get; set; } = null!;
    public string? Dimension { get; set; }
    
    public ICollection<ProductVariation> ProductVariations { get; set; } = null!;
}