using System.ComponentModel.DataAnnotations.Schema;

namespace CatalogService.Models.DataModels;

public record Manufacture
{
    public Manufacture()
    {
        Categories = new List<Category>();
    }

    public int Id { get; init; }
    public string Name { get; init; } = null!;
    [InverseProperty("Manufactures")] public ICollection<Category> Categories { get; init; } = null!;
}