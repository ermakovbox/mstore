using System.ComponentModel.DataAnnotations.Schema;

namespace CatalogService.Models.DataModels;

public record Category
{
    public Category()
    {
        Manufactures = new List<Manufacture>();
    }

    public int Id { get; init; }
    public string Name { get; init; } = null!;
    public ICollection<Product>? Products { get; init; }
    [InverseProperty("Categories")] public ICollection<Manufacture> Manufactures { get; init; } = null!;
}