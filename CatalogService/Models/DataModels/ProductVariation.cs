namespace CatalogService.Models.DataModels;

public record ProductVariation
{
    public Ulid Id { get; init; }
    public Ulid ProductId { get; init; }
    public string FullName { get; init; } = null!;
    public string? Image { get; init; }
    public decimal Price { get; init; } //надо изменить тип на double
    public ICollection<ProductVariationProperty>? Properties { get; init; }
}