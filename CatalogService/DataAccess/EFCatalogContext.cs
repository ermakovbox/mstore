using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CatalogService.DataAccess;

public class EFCatalogContext : DbContext
{
    public EFCatalogContext(DbContextOptions<EFCatalogContext> options) : base(options) { }
    
    public DbSet<ProductsLibary.Models.Product> Products => Set<ProductsLibary.Models.Product>();
    
    public DbSet<Models.DataModels.Product> DataProducts => Set<Models.DataModels.Product>();
    public DbSet<Models.DataModels.Manufacture> DataManufactures => Set<Models.DataModels.Manufacture>();
    public DbSet<Models.DataModels.Category> DataCategories => Set<Models.DataModels.Category>();
    public DbSet<Models.DataModels.ProductVariation> DataProductVariations => Set<Models.DataModels.ProductVariation>();
    public DbSet<Models.DataModels.ProductVariationProperty> DataProperties => Set<Models.DataModels.ProductVariationProperty>();
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var ulidConverter = new ValueConverter<Ulid, string>(
            v => v.ToString(),
            v => Ulid.Parse(v),
            new ConverterMappingHints(size: 26) // Рекомендуемый размер для строкового представления Ulid
        );

        modelBuilder
            .Entity<Models.DataModels.Product>()
            .Property(p => p.Id)
            .HasConversion(ulidConverter);
        
        modelBuilder
            .Entity<Models.DataModels.ProductVariation>()
            .Property(p => p.Id)
            .HasConversion(ulidConverter);
        
        modelBuilder
            .Entity<Models.DataModels.ProductVariation>()
            .Property(p => p.ProductId)
            .HasConversion(ulidConverter);
        
        modelBuilder
            .Entity<Models.DataModels.ProductVariationProperty>()
            .Property(p => p.Id)
            .HasConversion(ulidConverter);
        
        base.OnModelCreating(modelBuilder);
    }
}