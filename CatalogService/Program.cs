using System.Text;
using CatalogService.ApiServices;
using CatalogService.DataAccess;
using CatalogService.Interceptors;
using CatalogService.Repositories;
using CatalogService.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);
const string defaultConnectionString = "ConnectionStrings:DefaultConnection";

builder.Services.AddDbContext<EFCatalogContext>(options =>
{
    options.UseNpgsql(builder.Configuration[defaultConnectionString], 
        x => x.MigrationsAssembly(typeof(Program).Assembly.FullName));
});

builder.Services.AddDistributedMemoryCache(); //добавляем кеш в приложение
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30); // Время жизни сессии
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
}); //добавление сохранения сессии пользователя

builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IEFCatalogRepository, EFCatalogRepository>();
builder.Services.AddScoped<IEFManufactureRepository, EFManufactureRepository>();
builder.Services.AddScoped<IEFCategoryRepository, EFCategoryRepository>();
builder.Services.AddScoped<IEFProductRepository, EFProductRepository>();
builder.Services.AddScoped<IEFProductVariationRepository, EFProductVariationRepository>();

builder.Services.AddAuthorization();
builder.Services.AddAuthentication("Bearer")
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = "https://localhost:5001"; // Адрес IdentityServer
        options.RequireHttpsMetadata = true; // Используйте true в продакшене
        options.Audience = "CatalogApi"; // Идентификатор(apiresource) вашего API, должен соответствовать одному из настроенных в IdentityServer
        
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("verySecretKeyWhichShouldBeLongEnough")),
            ValidateIssuer = true,
            ValidIssuer = "https://localhost:5001",
            ValidateAudience = true,
            ValidAudience = "CatalogApi",
            ClockSkew = TimeSpan.Zero
        };
        
        
        options.Events = new JwtBearerEvents
        {
            OnAuthenticationFailed = context =>
            {
                // Логгирование ошибки аутентификации
                Console.WriteLine("Authentication failed: " + context.Exception.Message);
                return Task.CompletedTask;
            },
            OnTokenValidated = context =>
            {
                // Логгирование успешной валидации токена
                Console.WriteLine("Token validated.");
                return Task.CompletedTask;
            }
        };

        options.SaveToken = true; //токен будет сохранен в AuthenticationProperties;
    });

builder.Services.AddGrpc(options =>
{
    options.Interceptors.Add<AuthTokenLoggingInterceptor>();
});

// Add services to the container.
builder.Services.AddGrpc();

var app = builder.Build();

app.UseAuthentication(); // Добавляет аутентификацию
app.UseAuthorization();

// Configure the HTTP request pipeline.
app.MapGrpcService<CatalogApiService>();
app.MapGrpcService<ManufacturesApiService>();
app.MapGrpcService<CategoriesApiService>();
app.MapGrpcService<ProductsApiService>();
app.MapGrpcService<ProductVariationsApiService>();

app.MapGet("/",
    () =>
        "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();