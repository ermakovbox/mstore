using ApiService.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiService.Controllers;

public class BasketController : ControllerBase
{
    private List<Product> Basket { get; set; } = new();
    
    [HttpPost("api/basket/add")]
    public IActionResult AddToBasket(Product product)
    {
        Basket.Add(product);
        return Ok(Basket);
    }
}