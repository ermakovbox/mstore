using System.Text;
using GatewayService.Extensions;
using GatewayService.Interfaces;
using GatewayService.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddHttpContextAccessor();

builder.Services.AddDistributedMemoryCache(); //добавляем кеш в приложение
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30); // Время жизни сессии
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
}); //добавление сохранения сессии пользователя

builder.Services.AddAuthentication("Bearer")
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = "https://localhost:5001"; // Адрес IdentityServer
        options.RequireHttpsMetadata = true; // Используйте true в продакшене
        options.Audience = "BasketApi"; // Идентификатор(apiresource) вашего API, должен соответствовать одному из настроенных в IdentityServer
        
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("verySecretKeyWhichShouldBeLongEnough")),
            ValidateIssuer = true,
            ValidIssuer = "https://localhost:5001",
            ValidateAudience = true,
            ValidAudience = "BasketApi",
            ClockSkew = TimeSpan.Zero
        };
        
        
        options.Events = new JwtBearerEvents
        {
            OnAuthenticationFailed = context =>
            {
                // Логгирование ошибки аутентификации
                Console.WriteLine("Authentication failed: " + context.Exception.Message);
                return Task.CompletedTask;
            },
            OnTokenValidated = context =>
            {
                // Логгирование успешной валидации токена
                Console.WriteLine("Token validated.");
                return Task.CompletedTask;
            }
        };

        options.SaveToken = true; //токен будет сохранен в AuthenticationProperties;
    });

builder.Services.AddScoped<ITokenProvider, AppTokenProvider>();

builder.Services.AddCustomGrpcClient<BasketClient.BasketService.BasketServiceClient>(new Uri("https://localhost:7131"), "BasketApi");
builder.Services.AddCustomGrpcClient<CatalogClient.CatalogService.CatalogServiceClient>(new Uri("https://localhost:7298"), "CatalogApi");
builder.Services.AddCustomGrpcClient<CatalogClient.ManufacturesService.ManufacturesServiceClient>(new Uri("https://localhost:7298"), "CatalogApi");
builder.Services.AddCustomGrpcClient<CatalogClient.CategoriesService.CategoriesServiceClient>(new Uri("https://localhost:7298"), "CatalogApi");
builder.Services.AddCustomGrpcClient<CatalogClient.ProductsService.ProductsServiceClient>(new Uri("https://localhost:7298"), "CatalogApi");
builder.Services.AddCustomGrpcClient<CatalogClient.ProductVariationsService.ProductVariationsServiceClient>(new Uri("https://localhost:7298"), "CatalogApi");

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "JWT Authorization header using the Bearer scheme",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                },
                Scheme = "oauth2",
                Name = "Bearer",
                In = ParameterLocation.Header,
            },
            new List<string>()
        }
    });
});

var app = builder.Build();

app.UseAuthentication();
app.UseAuthorization();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSession();

app.UseHttpsRedirection();

app.MapControllers();

app.Run();