using CatalogClient;
using GatewayService.Models.DTOModels;
using Microsoft.AspNetCore.Mvc;

namespace GatewayService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ProductVariationsController : ControllerBase
{
    private readonly ProductVariationsService.ProductVariationsServiceClient _productVariationsServiceClient;

    public ProductVariationsController(ProductVariationsService.ProductVariationsServiceClient productVariationsServiceClient)
    {
        _productVariationsServiceClient = productVariationsServiceClient;
    }
    
    [HttpPost("create-product-variation")]
    public async Task<IActionResult> CreateProductVariation(ProductVariationDto productVariation)
    {
        var request = new CreateProductVariationRequest
        {
            ProductId = productVariation.ProductId,
            FullName = productVariation.FullName,
            Image = productVariation.Image,
            Price = (float)productVariation.Price
        };
        
        var response = await _productVariationsServiceClient.CreateProductVariationAsync(request);
        return Ok(response);
    }
    
    [HttpGet("get-product-variation-by-id/{id}")]
    public async Task<IActionResult> GetProductVariationById(string id)
    {
        var request = new GetProductVariationByIdRequest { Id = id };
        
        var response = await _productVariationsServiceClient.GetProductVariationByIdAsync(request);
        return Ok(response);
    }
    
    [HttpGet("get-product-variations-by-product-id/{productId}")]
    public async Task<IActionResult> GetProductVariationsByProductId(string productId)
    {
        var request = new GetProductVariationByProductIdRequest { ProductId = productId };
        
        var response = await _productVariationsServiceClient.GetProductVariationsByProductIdAsync(request);
        return Ok(response);
    }
}