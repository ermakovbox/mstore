using CatalogClient;
using Microsoft.AspNetCore.Mvc;

namespace GatewayService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CategoriesController : ControllerBase
{
    private readonly CategoriesService.CategoriesServiceClient _categoriesServiceClient;

    public CategoriesController(CategoriesService.CategoriesServiceClient categoriesServiceClient)
    {
        _categoriesServiceClient = categoriesServiceClient;
    }
    
    [HttpPost("create-category")]
    public async Task<IActionResult> CreateCategory(string category)
    {
        var request = new CreateCategoryRequest
        {
            Name = category
        };

        var response = await _categoriesServiceClient.CreateCategoryAsync(request);
        return Ok(response);
    }
    
    [HttpPut("add-category-to-manufacture")]
    public async Task<IActionResult> AddCategoryToManufacture(int manufactureId, int categoryId)
    {
        var request = new AddCategoryToManufactureRequest
        {
            ManufactureId = manufactureId,
            CategoryId = categoryId
        };

        var response = await _categoriesServiceClient.AddCategoryToManufactureAsync(request);
        return Ok(response);
    }
}