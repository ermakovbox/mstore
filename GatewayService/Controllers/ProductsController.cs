using CatalogClient;
using GatewayService.Models.DTOModels;
using Microsoft.AspNetCore.Mvc;

namespace GatewayService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ProductsController : ControllerBase
{
    private readonly ProductsService.ProductsServiceClient _productsServiceClient;

    public ProductsController(ProductsService.ProductsServiceClient productsServiceClient)
    {
        _productsServiceClient = productsServiceClient;
    }
    
    [HttpPost("create-product")]
    public async Task<IActionResult> CreateProduct(ProductDto product)
    {
        var request = new CreateProductBaseRequest()
        {
            Name = product.Name,
            Description = product.Description,
            CategoryIds = { product.Categories }
        };
        
        var response = await _productsServiceClient.CreateProductBaseAsync(request);
        return Ok(response);
    }
    
    [HttpGet("get-full-product-by-id/{id}")]
    public async Task<IActionResult> GetFullProductById(string id)
    {
        var request = new GetFullProductByIdRequest { Id = id };
        
        var response = await _productsServiceClient.GetFullProductByIdAsync(request);
        return Ok(response);
    }
}