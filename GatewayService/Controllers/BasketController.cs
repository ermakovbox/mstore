using BasketClient;
using Microsoft.AspNetCore.Mvc;

namespace GatewayService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BasketController : ControllerBase
{
    private readonly BasketService.BasketServiceClient _basketClient;

    public BasketController(BasketService.BasketServiceClient basketClient)
    {
        _basketClient = basketClient;
    }

    // [HttpGet("get-basket-by-id/{id}")]
    // public async Task<IActionResult> GetBasketById(string id)
    // {
    //     var request = new GetBasketByIdRequest { Id = id };
    //     var response = await _basketClient.GetBasketByIdAsync(request);
    //     return Ok(response);
    // }

    [HttpPost("add-product-to-basket")]
    public async Task<IActionResult> AddProductToBasket(AddProductToBasketRequest request)
    {
        var response = await _basketClient.AddProductToBasketAsync(request);
        return Ok(response);
    }

    // [HttpPost("remove-product-from-basket")]
    // public async Task<IActionResult> RemoveProductFromBasket(RemoveProductFromBasketRequest request)
    // {
    //     var response = await _basketClient.RemoveProductFromBasketAsync(request);
    //     return Ok(response);
    // }
    
}