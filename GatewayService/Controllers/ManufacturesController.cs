using CatalogClient;
using Microsoft.AspNetCore.Mvc;

namespace GatewayService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ManufacturesController : ControllerBase
{
    private readonly ManufacturesService.ManufacturesServiceClient _manufacturesServiceClient;

    public ManufacturesController(ManufacturesService.ManufacturesServiceClient manufacturesServiceClient)
    {
        _manufacturesServiceClient = manufacturesServiceClient;
    }
    
    [HttpGet("get-manufactures-by-name/{name}")]
    public async Task<IActionResult> GetManufacturesByName(string name)
    {
        var request = new GetManufacturesRequest { Name = name };
        var response = await _manufacturesServiceClient.GetManufacturesAsync(request);
        return Ok(response);
    }
    
    [HttpGet("get-manufacture-by-id/{id}")]
    public async Task<IActionResult> GetManufactureById(int id)
    {
        var request = new GetManufactureByIdRequest { Id = id };
        var response = await _manufacturesServiceClient.GetManufactureByIdAsync(request);
        return Ok(response);
    }
    
    [HttpPost("add-manufacture")]
    public async Task<IActionResult> AddManufacture(string manufacture)
    {
        if (string.IsNullOrWhiteSpace(manufacture))
        {
            return BadRequest("Manufacture name cannot be empty.");
        }
        
        var request = new CreateManufactureRequest
        {
            Name = manufacture
        };

        var response = await _manufacturesServiceClient.CreateManufactureAsync(request);
        return Ok(response);
    }
}