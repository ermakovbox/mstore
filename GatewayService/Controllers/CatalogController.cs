using CatalogClient;
using Microsoft.AspNetCore.Mvc;

namespace GatewayService.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CatalogController : ControllerBase
{
    private readonly CatalogService.CatalogServiceClient _catalogServiceClient;

    public CatalogController(CatalogService.CatalogServiceClient catalogServiceClient)
    {
        _catalogServiceClient = catalogServiceClient;
    }
    
    [HttpGet("get-products-by-name/{name}")]
    public async Task<IActionResult> GetProductsByName(string name)
    {
        var request = new GetProductsRequest { Name = name };
        var response = await _catalogServiceClient.GetProductsByNameAsync(request);
        return Ok(response);
    }
    
    [HttpGet("get-product-by-id/{id}")]
    public async Task<IActionResult> GetProductById(string id)
    {
        var request = new GetProductByIdRequest { Id = id };
        var response = await _catalogServiceClient.GetProductByIdAsync(request);
        return Ok(response);
    }
}