using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using GatewayService.Interfaces;
using Microsoft.IdentityModel.Tokens;

namespace GatewayService.Services;

public class AppTokenProvider : ITokenProvider
{
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly string? _sessionId;

    public AppTokenProvider(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
        _sessionId = httpContextAccessor.HttpContext?.Session.Id;
    }

    public Task<string> GetTokenAsync(string apiResource, CancellationToken cancellationToken)
    {
        if (_httpContextAccessor.HttpContext is null)
        {
            Console.WriteLine("HttpContext is null. Using internal JWT token.");
        
            // Возвращаем внутренний токен, так как HttpContext не доступен
            return Task.FromResult(GenerateInternalJwtToken(apiResource));
        }
        
        var isUserAuthenticated = _httpContextAccessor.HttpContext.User.Identity is { IsAuthenticated: true };
        
        var token = isUserAuthenticated
            ? GetUserJwtToken()
            : GenerateInternalJwtToken(apiResource);
        
        if (string.IsNullOrEmpty(token))
        {
            throw new ArgumentNullException(nameof(token));
        }
        
        return Task.FromResult(token);
    }

    private string? GetUserJwtToken()
    {
        return _httpContextAccessor.HttpContext?.Request.Headers.Authorization.FirstOrDefault()?.Replace("Bearer ", "");
    }

    private string GenerateInternalJwtToken(string apiResource)
    {
        var issuer = "https://localhost:5001"; // Издатель токена, должен совпадать с доменом IdentityServer

        var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("verySecretKeyWhichShouldBeLongEnough")), SecurityAlgorithms.HmacSha256Signature);
        
        if (string.IsNullOrEmpty(_sessionId))
        {
            throw new ArgumentNullException(nameof(_sessionId));
        }

        var claims = new List<Claim>
        {
            new Claim(JwtRegisteredClaimNames.Sub, _sessionId),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            // Можете добавить дополнительные клеймы, если это необходимо
        };

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.UtcNow.AddHours(1), // Срок действия токена
            SigningCredentials = signingCredentials,
            Audience = apiResource,
            Issuer = issuer
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var token = tokenHandler.CreateToken(tokenDescriptor);
        
        _httpContextAccessor.HttpContext?.Session.SetString(_sessionId, tokenHandler.WriteToken(token));
        return tokenHandler.WriteToken(token);
    }
}