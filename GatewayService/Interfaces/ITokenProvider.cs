namespace GatewayService.Interfaces;

public interface ITokenProvider
{
    Task<string> GetTokenAsync(string apiResource, CancellationToken cancellationToken);
}