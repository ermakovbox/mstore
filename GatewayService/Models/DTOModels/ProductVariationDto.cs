namespace GatewayService.Models.DTOModels;

public class ProductVariationDto
{
    public string ProductId { get; set; } = null!;
    public string FullName { get; set; } = null!;
    public string? Image { get; set; }
    public decimal Price { get; set; }
}