namespace GatewayService.Models.DTOModels;

public class ProductDto
{
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
    public IEnumerable<int> Categories { get; set; } = null!;
}