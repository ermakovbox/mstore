using GatewayService.Interfaces;
using Grpc.Core;

namespace GatewayService.Extensions;

public static class GrpcClientExtension
{
    public static void AddCustomGrpcClient<TClient>(
        this IServiceCollection services, Uri address, string apiResource) where TClient : ClientBase<TClient>
    {
        services.AddGrpcClient<TClient>(o => o.Address = address)
            .AddCallCredentials(async (context, metadata, serviceProvider) =>
            {
                var provider = serviceProvider.GetRequiredService<ITokenProvider>();
                var token = await provider.GetTokenAsync(apiResource, context.CancellationToken);
                metadata.Add("Authorization", $"Bearer {token}");
            });
    }
}