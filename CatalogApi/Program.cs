using CatalogApi.DataAccess;
using CatalogApi.Repositories;
using CatalogApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
const string defaultConnectionString = "ConnectionStrings:DefaultConnection";

builder.Services.AddControllers();

builder.Services.AddDbContext<EFCatalogContext>(options =>
{
    options.UseNpgsql(builder.Configuration[defaultConnectionString], 
        x => x.MigrationsAssembly(typeof(Program).Assembly.FullName));
});

builder.Services.AddScoped<IEFCatalogRepository, EFCatalogRepository>();

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapControllers();

app.Run();