using CatalogApi.DataAccess;
using CatalogApi.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using ProductsLibary.Models;

namespace CatalogApi.Repositories;

public class EFCatalogRepository : IEFCatalogRepository
{
    private readonly EFCatalogContext _context;

    public EFCatalogRepository(EFCatalogContext context)
    {
        _context = context;
    }
    
    public async Task AddProductAsync(Product product)
    {
        await _context.Products.AddAsync(product);
        await _context.SaveChangesAsync();
    }
    
    public async Task<IList<Product>> GetProductsAsync()
    {
        return await _context.Products.ToListAsync();
    }
    
    public async Task<Product?> GetProductByIdAsync(Guid id)
    {
        return await _context.Products.FindAsync(id);
    }
}