using ProductsLibary.Models;

namespace CatalogApi.Repositories.Interfaces;

public interface IEFCatalogRepository
{
    public Task AddProductAsync(Product product);
    
    public Task<IList<Product>> GetProductsAsync();
    
    public Task<Product?> GetProductByIdAsync(Guid id);
}