using CatalogApi.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using ProductsLibary.Models;

namespace CatalogApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CatalogController : ControllerBase
{
    private readonly IEFCatalogRepository _repository;

    public CatalogController(IEFCatalogRepository repository)
    {
        _repository = repository;
    }
    
    [HttpPost("add-product")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
    public async Task<IActionResult> AddProductAsync([FromBody] Product product)
    {
        await _repository.AddProductAsync(product);
        
        return StatusCode(StatusCodes.Status201Created, product);
    }
    
    [HttpGet("get-products")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
    public async Task<IActionResult> GetProductsAsync()
    {
        var products = await _repository.GetProductsAsync();
        
        return Ok(products);
    }
    
    [HttpGet("get-product-by-id/{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
    public async Task<IActionResult> GetProductByIdAsync(Guid id)
    {
        var product = await _repository.GetProductByIdAsync(id);
        
        if (product is null)
        {
            return NotFound("Product not found");
        }
        
        return Ok(product);
    }
}