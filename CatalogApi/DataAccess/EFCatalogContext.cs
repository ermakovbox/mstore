using Microsoft.EntityFrameworkCore;
using ProductsLibary.Models;

namespace CatalogApi.DataAccess;

public class EFCatalogContext : DbContext
{
    public EFCatalogContext(DbContextOptions<EFCatalogContext> options) : base(options) { }
    
    public DbSet<Product> Products => Set<Product>();
}